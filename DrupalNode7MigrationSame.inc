<?php
/**
 *
 */

abstract class DrupalNode7MigrationSame extends DrupalNode7Migration {
  private $top_book = FALSE;
  function __construct($arguments) {
    $arguments['source_connection'] = 'smt_hosted';
    $arguments['source_version'] = 7;
    $arguments['source_type'] = $arguments['same_content_type'];
    $arguments['destination_type'] = $arguments['same_content_type'];
    parent::__construct($arguments);

    $this->addAllFields();
  }

  protected function addAllFields($exclude = array('body')) {
    //dpm(field_info_instances('node', $this->destinationType));
    $field_names = array_keys(field_info_instances('node', $this->destinationType));
    $field_names = array_diff($field_names, $exclude);
    foreach ($field_names as $field_name) {
      $field_mapping = $this->addFieldMapping($field_name, $field_name);
      $field_info = field_info_field($field_name);
      if (!empty($field_info)) {
        foreach ($field_info['columns'] as $key => $value) {
          $this->addFieldMapping("$field_name:$key", "$field_name:$key");
        }
        if ($field_info['type'] == 'file') {
          $field_mapping->sourceMigration('Files');
          $this->addFieldMapping("$field_name:file_class")
            ->defaultValue('MigrateFileFid');
        }

      }

    }
  }
  protected function mapFileFields($field_names, $source_dir) {

  }




}
